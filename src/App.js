import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
    this.changeToMonthData = this.changeToMonthData.bind(this);
    this.changeToDateData = this.changeToDateData.bind(this);
  }
  changeToMonthData() {
    this.setState({
      data: this.monthData
    })
  }
  changeToDateData() {
    this.setState({
      data: this.dateData
    })
  }
  convertToMonthData(data) {
    var handleData = data.map((item, index) => {
      var month = item.time.split("-")[1]
      return { ...item, month }
    })
    var monthArray = handleData.map(item => {
      return item.month
    }).filter(function (elem, index, self) {
      return index === self.indexOf(elem);
    });
    var monthData = [];
    for (let i = 0; i < monthArray.length; i++) {
      var sum = 0;
      var pricesData = [];
      var openPrices = [];
      var closePrices = [];
      var lowestPrices = [];
      var highestPrices = [];
      handleData.forEach((item, index) => {
        if (item.month == monthArray[i]) {
          sum = sum + item.volume;
          openPrices.push(item.prices[0]);
          closePrices.push(item.prices[1]);
          lowestPrices.push(item.prices[2]);
          highestPrices.push(item.prices[3]);
        }
      })
      pricesData = [openPrices[0], closePrices[closePrices.length - 1], Math.min(...lowestPrices), Math.max(...highestPrices)]
      monthData.push({ time: monthArray[i], volume: sum, prices: pricesData })
    }
    this.monthData = monthData;
  }
  componentDidMount() {
    fetch('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT&apikey=demo')
      .then(response => response.json())
      .then(json => {
        var myObj = JSON.parse(JSON.stringify(json["Time Series (Daily)"]));
        var dateData = Object.keys(myObj).map(item => {
          return {
            time: item,
            volume: parseInt(myObj[item]['6. volume']),
            prices: [parseFloat(myObj[item]['1. open']), parseFloat(myObj[item]['4. close']), parseFloat(myObj[item]['3. low']), parseFloat(myObj[item]['2. high'])]
          }
        })
        this.dateData = dateData;
        this.setState({
          data: this.dateData
        })
        this.convertToMonthData(dateData);
      });
  }
  getOption = () => {
    var timeData = this.state.data.map(item => {
      return item.time
    })
    var volumeData = this.state.data.map(item => {
      return item.volume
    })
    var pricesData = this.state.data.map(item => {
      return item.prices
    })
    return {
      title: {
        text: 'ECharts entry example'
      },
      tooltip: {},
      legend: {
        data: ['Volume', 'Prices']
      },
      grid: [{
        left: '8%',
        right: '1%',
        top: '35%',
        height: '55%'
      },
      {
        left: '8%',
        right: '1%',
        height: '20%'
      }],
      xAxis: [{
        type: 'category',
        data: timeData
      },
      {
        type: 'category',
        gridIndex: 1,
        data: timeData
      }],
      yAxis: [{},
      {
        gridIndex: 1,
        scale: true
      }],
      series: [{
        name: 'Volume',
        type: 'bar',
        data: volumeData
      },
      {
        name: 'Prices',
        type: 'candlestick',
        xAxisIndex: 1,
        yAxisIndex: 1,
        data: pricesData
      }
      ]
    };
  }
  render() {
    return (
      <div>
        <ReactEcharts option={this.getOption()} style={{ height: '700px', width: '100%' }}
          className='react_for_echarts' />
        <button onClick={this.changeToMonthData}>Change to Month</button>
        <button onClick={this.changeToDateData}>Change to Date</button>
      </div>
    );
  }
}

export default App;
